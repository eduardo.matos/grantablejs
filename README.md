# GrantableJS

Verifies permissions globally

## Usage

```js
import { grant, deny, granted, denied, denyAll } from 'grantable';

denied('spam'); // true
granted('spam'); // false

grant('spam');
 
denied('spam'); // false
granted('spam'); // true

deny('spam');

denied('spam'); // true
granted('spam'); // false

grant('foo');
denyAll();

denied('foo'); // true
granted('foo'); // false
```

## Building

```sh
npm run build
```

## Tests

```sh
npm test
```
