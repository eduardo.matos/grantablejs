interface BoolMap {
  [key: string]: boolean;
}

const permissions: BoolMap = {};

// Grants a given permission
export function grant(permission: string) {
  permissions[permission] = true;
}

// Denies a given permission
export function deny(permission: string) {
  delete permissions[permission];
}

export function denyAll() {
  for (const key in permissions) {
    if (permissions.hasOwnProperty(key)) {
      delete permissions[key];
    }
  }
}

// Check if a given permission is granted
export function granted(permission: string): boolean {
  return !!permissions[permission];
}

// Check if a given permission is denied.
// If it's not granted, then it's denied.
export function denied(permission: string): boolean {
  return !permissions[permission];
}
