import { expect } from 'chai';
import { grant, deny, granted, denied, denyAll } from '../src/grantable';

beforeEach(denyAll);

it('Deny permission by default', () => {
  expect(denied('spam')).to.be.true;
});

it('Grants permission', () => {
  grant('spam');
  expect(granted('spam')).to.be.true;
});

it('Denies permission', () => {
  grant('spam');
  deny('spam');

  expect(granted('spam')).to.be.false;
  expect(denied('spam')).to.be.true;
});

it('Denies all permissions', () => {
  grant('spam');
  grant('spam2');
  denyAll();

  expect(granted('spam')).to.be.false;
  expect(denied('spam')).to.be.true;

  expect(granted('spam2')).to.be.false;
  expect(denied('spam2')).to.be.true;
});
